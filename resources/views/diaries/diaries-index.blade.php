{{-- DUMMY DATA --}}
@php
    $date_current = Carbon\Carbon::createFromFormat('Y-m-d', $date);
@endphp

@extends('layouts.sidebar-wrapper')

@section('title') Diaries - @parent @endsection
@section('body')
    @parent
    <script>
        function carouseler() {
            $('#carouselExampleIndicators *[data-slide-to]').on('click', function(){
                to = parseInt($(this).attr('data-slide-to'));
                $('#carouselExampleIndicators2').carousel(to);
            });
            $('#carouselExampleIndicators .carousel-control-prev').on('click', function(){
                $('#carouselExampleIndicators2').carousel('prev');
            });
            $('#carouselExampleIndicators .carousel-control-next').on('click', function(){
                $('#carouselExampleIndicators2').carousel('next');
            });
        }

        function centerNav() {

        }

        carouseler();

        $('.toast').toast('show');
    </script>
@endsection
@section('content')
    <div class="mx-auto px-0 px-sm-2 px-md-3 px-lg-4" style="max-width: 1200px">

        @include('diaries.diaries-navdate')
        @include('partials.toast')

        <div class="row mt-md-3 mt-lg-0">

            <div class="col-md-7 p-0">
                {{-- CARD --}}
                <div id="carouselExampleIndicators" class="carousel slide mt-4" data-ride="carousel" data-interval="0">
                    <ol class="carousel-indicators">
                        @foreach ($diaries as $diary)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration - 1 }}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($diaries as $diary)
                        <div class="carousel-item rounded @if($loop->iteration == count($diaries)) active @endif">
                            <img class="d-block w-100 rounded" src="{{ url("app/photos/{$diary['photo']}") }}">
                            <div class="carousel-caption rounded-top w-100 m-0 p-4 text-left"
                                style="top: 0; left: 0; bottom: unset; right: 8rem; background: linear-gradient(rgba(0, 0, 0, 1), rgba(0, 0, 0, 0));">
                                <h4 class="mb-0 text-white">{{ substr($diary['time'], 0, 5) }}</h4>
                                <h1 class="mb-0 text-white">{{ $diary['food'] }}</h4>
                            </div>
                            <h4 class="p-4" style="position: absolute; top: 0; right: 0; z-index:10;">
                                <a class="text-white" href="#" data-toggle="modal" data-target="#diaries-edit-{{ $diary['id'] }}">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                    </svg>
                                </a>
                            </h4>
                        </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div class="col-md-5 p-0">
                <div id="carouselExampleIndicators2" class="carousel slide mt-2 mt-md-4" data-ride="carousel" data-interval="0">
                    <div class="carousel-inner">
                        @foreach ($diaries as $diary)
                        <div class="carousel-item @if($loop->iteration == count($diaries)) active @endif">
                            <div class="bg-white rounded mx-1">
                                <div class="d-flex my-3">
                                    <div class="text-primary mx-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="1.5rem" height="1.5rem" fill="currentColor" version="1.1" viewBox="0 0 24 24" xml:space="preserve">
                                            <g transform="matrix(.02449 0 0 .02449 -.24488 -.2449)"><path d="m476.8 302.8v-241.2c0.1-28.4-23-51.5-51.5-51.5-14.3 0-27.2 5.7-36.6 15.1-9.3 9.3-15.1 22.2-15 36.5l-0.2 210.1c0 8.5-3.5 16.2-9.1 21.8s-13.3 9.1-21.8 9.1c-17.2 0.1-31-13.7-30.9-30.9l-0.1-210.2c0.1-28.4-23-51.5-51.5-51.5-14.3 0-27.2 5.8-36.5 15.1s-15.1 22.2-15 36.4l-0.1 210.2c0.1 8.6-3.4 16.3-9 21.9s-13.3 9-21.9 9c-17 0-30.9-13.9-30.9-30.9l-0.1-210.2c0.1-28.5-23-51.6-51.6-51.6-14.3 0-27.2 5.8-36.5 15.1s-15.1 22.2-15 36.4v241.2c0 85.5 95.8 131.1 135.7 199.4v299.7l-0.1 107c0 44.7 36.4 81.1 81.1 81.1 22.4 0 42.6-9.1 57.3-23.8s23.7-34.9 23.7-57.2l-0.2-106.2v-299.3c39.7-69.7 135.8-115.2 135.8-200.6z"/><path d="m956.5 255c0-135.3-94-245-210-245s-210 109.7-210 245c0 104.8 56.4 194.2 135.7 229.2v317.7l-0.1 107c0 44.7 36.4 81.1 81.1 81.1 22.4 0 42.6-9.1 57.3-23.8s23.7-34.9 23.7-57.2l-0.1-106.3v-324.9c72.2-38.8 122.4-123.9 122.4-222.8z"/></g>
                                        </svg>
                                    </div>
                                    <p>
                                        {{ floatval($diary['amount']) }} {{ $diary['portion'] }}
                                    </p>
                                </div>
                                <div class="d-flex my-3">
                                    <div class="text-primary mx-3">
                                        <svg width="1.5rem" height="1.5rem" viewBox="0 0 16 16" class="bi bi-bookmark-heart-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4 0a2 2 0 0 0-2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4zm4 4.41c1.387-1.425 4.854 1.07 0 4.277C3.146 5.48 6.613 2.986 8 4.412z"/>
                                        </svg>
                                    </div>
                                    <p>
                                        <?= $diary['note'] ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @foreach($diaries as $item)
            @include('diaries.diaries-edit')
            @include('diaries.diaries-delete')
        @endforeach


        <div class="mt-5" style="position: relative; text-align: right;margin-bottom: 5rem;">
            <span
            style=" position: fixed; bottom: 1rem; transform: translate(-100%);" class="mr-3">
                <button
                    type="button"
                    class="btn btn-primary bg-primary shadow-lg rounded-circle p-0 mr-3"
                    data-toggle="modal"
                    data-target="#add-diaries"
                    style="width: 3.5rem; height: 3.5rem; font-size: 2rem;"
                    >
                    &plus;
                </button>
            </span>
            <div class="text-center">
                <p class="text-muted px-4">
                    Tekan tombol + untuk menambah data
                </p>
            </div>
        </div>

        @include('diaries.diaries-add')

    </div>
@endsection
