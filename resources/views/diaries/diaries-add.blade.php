<!-- Scrollable modal -->
<div
    class="modal fade"
    id="add-diaries"
    tabindex="-1"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <form class="modal-content rounded" action="{{ url("app/diaries/$date_url/add") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="date" value="{{ $date }}">
            <div class="modal-header">
                <h5 class="modal-title">Tambah catatan</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="food">Makanan/Minuman</label>
                    <input class="form-control rounded" type="text" name="food" id="food" required/>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <label for="amount">Jumlah</label>
                        <input class="form-control rounded" type="number" name="amount" id="amount" required/>
                    </div>
                    <div class="col">
                        <label for="portion">Porsi</label>
                        <input class="form-control rounded" type="text" name="portion" id="portion" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="time">Jam</label>
                    <input class="form-control rounded" type="time" name="time" id="time" required/>
                </div>
                <div class="form-group">
                    <label for="note">Catatan</label>
                    <textarea class="form-control rounded" name="note" id="note" rows="4"></textarea>
                </div>
                <div class="form-group">
                    <label for="photo">Foto</label>
                    <div class="custom-file">
                        <label class="custom-file-label" for="photo">Foto</label>
                        <input class="custom-file-input" accept="image/*" capture type="file" name="photo" id="photo" placeholder="Pilih Foto"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">
                    Batal
                </button>
                <button class="btn btn-primary">
                    TAMBAH
                </button>
            </div>
        </form>
    </div>
</div>
