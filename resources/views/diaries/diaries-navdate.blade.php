{{-- Navigasi Bulan dan Tahun --}}

<div class="d-flex flex-wrap my-2 mx-3 mx-sm-0">

    {{-- Navigasi Bulan --}}
    <div class="dropdown show mr-3">

        {{-- Dropdown Button --}}
        <a class="btn btn-secondary px-1 bg-light border-0 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
        style="font-size: 1.5rem;">
          {{ $date_current->format('F') }}
        </a>

        {{-- Dropdown Menu --}}
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php
            loopMonth($date_current, function($i, $j){
            ?>
                <a class="dropdown-item @if($i->month == $j->month) bg-primary text-white @endif @if($i->month == date('j')) bg-dark text-white @endif"
                href="{{ $i->format('dmY') }}">
                    {{ $i->format('F') }}
                </a>
            <?php
            });
            ?>
        </div>
    </div>

    {{-- Navigasi Tahun --}}
    <div class="dropdown show">

        {{-- Dropdown Button --}}
        <a class="btn btn-secondary px-1 bg-light border-0 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
        style="font-size: 1.5rem;">
            {{ $date_current->year }}
        </a>

        {{-- Dropdown Menu --}}
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php
            loopYears($date_current, function($i, $j, $k){
            ?>
                <a class="dropdown-item @if($i == $j) bg-primary text-white @endif @if($i == date('Y')) bg-dark text-white @endif"
                    href="{{ $k->format('dm') . $i }}">
                    {{ $i }}
                </a>
            <?php
            });
            ?>
        </div>
    </div>
</div>

{{-- Navigasi Tanggal --}}
<ul class="nav flex-nowrap" id="date-scroller" style="overflow-x: auto;">
    <?php
    loopDays($date_current, function($class_name, $date_loop) {
    ?>
        <li class="nav-item mx-0">
            <a class="nav-link {{ $class_name }}"
            href="{{ $date_loop->format('dmY') }}">
                {{ $date_loop->day }}
            </a>
        </li>
    <?php
    });
    ?>
</ul>
