<!-- Scrollable modal -->
<div
    class="modal fade"
    id="diaries-delete-{{ $item['id'] }}"
    tabindex="-1"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered px-4">
        <form class="modal-content rounded" action="{{ url("app/diaries/$date_url/delete") }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $item['id'] }}">
            <div class="modal-header">
                <h5 class="modal-title">Hapus</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Data yang telah dihapus tidak dapat dikembalikan lagi. Anda yakin ingin menghapus data ini?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">
                    Batal
                </button>
                <button type="submit" class="btn btn-primary">
                    Hapus
                </button>
            </div>
        </form>
    </div>
</div>
