<!-- Scrollable modal -->
<div
    class="modal fade"
    id="diaries-edit-{{ $item['id'] }}"
    tabindex="-1"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <form class="modal-content rounded" action="{{ url("app/diaries/$date_url/update") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $item['id'] }}">
            <input type="hidden" name="date" value="{{ $date }}">
            <div class="modal-header d-flex flex-nowrap">
                <h5 class="modal-title truncate">Ubah<br/><small>{{ $item['time'] }} - {{ $item['food'] }}</small></h5>
                <h5>
                    <a class="text-primary" href="#" data-toggle="modal" data-target="#diaries-delete-{{ $item['id'] }}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                        </svg>
                    </a>
                </h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="food">Makanan/Minuman</label>
                    <input class="form-control rounded" type="text" name="food" id="food"
                    value="{{ $item['food'] }}"/>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <label for="amount">Jumlah</label>
                        <input class="form-control rounded" type="number" name="amount" id="amount"
                        value="{{ $item['amount'] }}"/>
                    </div>
                    <div class="col">
                        <label for="portion">Porsi</label>
                        <input class="form-control rounded" type="text" name="portion" id="portion"
                        value="{{ $item['portion'] }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="time">Jam</label>
                    <input class="form-control rounded" type="time" name="time" id="time"
                    value="{{ $item['time'] }}"/>
                </div>
                <div class="form-group">
                    <label for="note">Catatan</label>
                    <textarea class="form-control rounded" name="note" id="note" rows="4">{{ $item['note'] }}</textarea>
                </div>
                <div class="form-group">
                    <label for="photo">Foto</label>
                    <div class="custom-file">
                        <label class="custom-file-label" for="photo">Foto</label>
                        <input class="custom-file-input" accept="image/*" capture type="file" name="photo" id="photo" placeholder="Pilih Foto"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">
                    Batal
                </button>
                <button class="btn btn-primary">
                    Simpan
                </button>
            </div>
        </form>
    </div>
</div>
