@extends('layouts.master')

@section('title') Login - @parent @endsection

@section('body')

<div class="container" style="max-width: 420px">
    <div class="text-center pt-5 pb-4 mt-4">
        <h1><img src="{{ url('img/my-appetit.png') }}" width="64" height="64" class="d-inline-block align-middle" alt="" loading="lazy">
        <span class="align-middle ml-n1 text-primary" style="font-family: 'Bubblegum Sans', cursive; text-transform: none;">
            PPetit
        </span></h1>
    </div>
    <form method="POST" class="mt-1">
        @csrf
        <div class="form-group">
            <h4>Buat Akun</h4>
        </div>
        <div class="form-group my-4">
            <label for="name">Nama</label>
            <input class="form-control rounded" type="text" name="name" id="name" required/>
        </div>
        <div class="form-group my-4">
            <label for="email">E-mail</label>
            <input class="form-control rounded" type="email" name="email" id="email" required/>
        </div>
        <div class="form-group my-4">
            <label for="password">Kata sandi</label>
            <input class="form-control rounded" type="password" name="password" id="password" required/>
        </div>
        <div class="form-group my-4">
            <label for="repassword">Konfirmasi Kata sandi</label>
            <input class="form-control rounded" type="password" name="repassword" id="repassword" required/>
        </div>
        @error('email')
        <div class="text-danger my-2">{{ $message }}</div>
        @enderror
        <div class="form-group d-flex align-items-center">
            <a href="{{ route('register') }}">Belum punya akun?</a>
            <button type="submit" class="ml-auto btn btn-primary rounded">
                MASUK
            </button>
        </div>
    </form>

</div>
@parent
@endsection
