@extends('layouts.master')

@section('body')
    @include('partials.navbar')
    <div class="d-flex" id="wrapper">
        <div class="bg-light" id="sidebar-wrapper">
            <div class="list-group list-group-flush" style="overflow-x:hidden; overflow-y: auto; height: calc(100vh - 128px);">
                <a href="{{ url('app/diaries') }}" class="list-group-item list-group-item-action active">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="mb-1 mr-3 bi bi-book-half" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                    <span>
                        Buku Harian
                    </span>
                </a>
                <a href="#" class="list-group-item list-group-item-action disabled">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="mb-1 mr-3 bi bi-file-richtext-fill"
                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM7 4.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0zm-.861 1.542l1.33.886 1.854-1.855a.25.25 0 0 1 .289-.047l1.888.974V7.5a.5.5 0 0 1-.5.5H5a.5.5 0 0 1-.5-.5V7s1.54-1.274 1.639-1.208zM5 9a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z" />
                    </svg>
                    <span>
                        Resep
                    </span>
                </a>
            </div>
            <div class="p-2 d-flex flex-nowrap align-items-center" style="position: relative; overflow:hidden; width:15rem;">
                <img src="https://sibilindia.com/img/aimg/pic.svg" alt="userpic" class="rounded-circle img-fluid"
                    style="width: 2.5rem">
                <div class="ml-2 mr-4 text-truncate">
                    {{ Auth::user()->name }}
                </div>
                <a href="{{ route('logout') }}" class="mr-2" style="position: absolute; right:0; top:50%;
                transform: translateY(-50%);"
                data-toggle="tooltip" data-placement="top" title="Keluar">
                    <svg width="1.25rem" height="1.25rem" viewBox="0 0 16 16" class="bi bi-box-arrow-in-right"
                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z" />
                        <path fill-rule="evenodd"
                            d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z" />
                    </svg>
                </a>
            </div>
        </div>

        <div id="page-content-wrapper">
            @yield('content')
        </div>

    </div>

    @parent
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });

    </script>
@endsection
