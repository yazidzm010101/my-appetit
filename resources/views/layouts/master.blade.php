<!DOCTYPE html>
<html lang="en">
<head>
    @section('head')

        {{-- meta --}}
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        {{-- google fonts --}}
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bubblegum+Sans&display=swap" rel="stylesheet">

        {{-- styles --}}
        <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('css/index.min.css') }}">
        <title>@section('title') APPetit @show</title>
    @show
</head>
<body>
    @section('body')
        <script src="{{ url('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ url('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ url('js/index.js') }}"></script>
    @show
</body>
</html>
