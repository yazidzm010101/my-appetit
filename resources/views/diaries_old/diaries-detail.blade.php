<div id="diary-{{ $item['id'] }}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-0" style="background-color: rgba(0, 0, 0, 0);">
            <div class="modal-body">
                <div style="position: relative;">
                    <img src="{{ url("app/photos/{$item['photo']}") }}" alt="gambarku"
                    class="img-fluid rounded-top" style="width: 100%">
                    <div class="d-flex text-white rounded-top"
                    style="position: absolute; top: 0; left: 0; background-color: rgba(0, 0, 0, 0.8); width: 100%;
                    background: linear-gradient(rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0));">
                        <h1 class="m-0 p-4 text-truncate text-white">
                            {{ $item['food'] }}<br/>
                            <small>
                                {{ substr($item['time'], 0, 5) }}
                            </small>
                        </h1>
                        <h5 class="ml-auto p-4">
                            <a class="mx-2 text-white" href="#" data-toggle="modal" data-target="#diaries-edit-{{ $item['id'] }}" data-dismiss="modal">
                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="align-middle bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                </svg>
                            </a>
                            <a class="mx-2 text-danger" href="#">
                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="align-middle bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                </svg>
                            </a>
                        </h5>
                    </div>
                </div>
                <div class="bg-white rounded-bottom p-4 d-flex align-items-center text-primary">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class=" p-1 mr-3 bi bi-bookmark-heart-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4 0a2 2 0 0 0-2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4zm4 4.41c1.387-1.425 4.854 1.07 0 4.277C3.146 5.48 6.613 2.986 8 4.412z"/>
                    </svg>
                    <h5 class="m-0" style="text-transform: none;">
                        {{ $item['note'] }}
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>
