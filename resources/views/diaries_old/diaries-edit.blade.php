<!-- Scrollable modal -->
<div
    class="modal fade"
    id="diaries-edit-{{ $item['id'] }}"
    tabindex="-1"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <form class="modal-content rounded" action="{{ url("app/diaries/$date_url/add") }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $item['id'] }}">
            <input type="hidden" name="date" value="{{ $date }}">
            <div class="modal-header">
                <h5 class="modal-title">Ubah<br/><small>{{ $item['time'] }} - {{ $item['food'] }}</small></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="food">Makanan/Minuman</label>
                    <input class="form-control rounded" type="text" name="food" id="food"
                    value="{{ $item['food'] }}"/>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <label for="amount">Jumlah</label>
                        <input class="form-control rounded" type="number" name="amount" id="amount"
                        value="{{ $item['amount'] }}"/>
                    </div>
                    <div class="col">
                        <label for="portion">Porsi</label>
                        <input class="form-control rounded" type="text" name="portion" id="portion"
                        value="{{ $item['portion'] }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="time">Jam</label>
                    <input class="form-control rounded" type="time" name="time" id="time"
                    value="{{ $item['time'] }}"/>
                </div>
                <div class="form-group">
                    <label for="note">Catatan</label>
                    <textarea class="form-control rounded" name="note" id="note" rows="4">{{ $item['note'] }}</textarea>
                </div>
                <div class="form-group">
                    <label for="photo">Foto</label>
                    <div class="custom-file">
                        <label class="custom-file-label" for="photo">Foto</label>
                        <input class="custom-file-input" accept="image/*" capture type="file" name="photo" id="photo" placeholder="Pilih Foto"/>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
