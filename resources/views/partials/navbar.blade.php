<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="btn btn-light d-inline-block align-middle text-center p-0 mr-3 rounded-circle" id="menu-toggle" type="button"
    style="width: 2rem; height: 2rem;">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand text-primary ml-auto mr-auto" href="#"
        style="position: fixed; left: 50%; transform: translateX(-50%);">
        <img src="{{ url('img/my-appetit.png') }}" width="30" height="30" class="d-inline-block align-middle" alt="" loading="lazy">
        <span class="align-middle ml-n1">
            PPetit
        </span>
    </a>
</nav>
