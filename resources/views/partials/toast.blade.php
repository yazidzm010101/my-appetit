<div aria-live="polite" aria-atomic="true" style="position: relative;">
    <!-- Position it -->
    <div style="position: absolute; z-index:1000; top: 0; right: 0;">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="4000">
                <div class="toast-header">
                {{-- <img src="..." class="rounded mr-2" alt="..."> --}}
                <strong class="mr-auto text-danger">Error</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="toast-body">
                    {{$error}}
                </div>
            </div>
        @endforeach
        @endif
    </div>
</div>
