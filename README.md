# MY APPETIT
### A Food Diary Web App
My Appetit adalah aplikasi web yang digunakan untuk menulis buku harian tentang apa saja yang sudah dikonsumsi setiap hari.

## CORE FEATURES
Menulis catatan harian yang berisi
- Tanggal dan jam
- Jenis/nama makanan yang dikonsumsi
- Jumlah porsi/satuan    
- Catatan/keterangan tambahan

## OPTIONAL FEATURES
Berikut ini fitur opsional
- Dark Mode Support

## TOOLS & FRAMEWORK
Di bawah ini adalah peralatan yang kami gunakan untuk membangun aplikasi tersebut
- PHP with Laravel
- MySQL
- Bootstrap