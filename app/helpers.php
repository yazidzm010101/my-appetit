<?php

function loopDays($date_current, $callback)
{
    $date_start = new Carbon\Carbon("first day of {$date_current->format('F Y')}");
    $date_end = new Carbon\Carbon("last day of {$date_current->format('F Y')}");
    $date_loop = new Carbon\Carbon($date_start->format('Y-m-d'));

    while ($date_loop->lessThanOrEqualTo($date_end)) {
        $class_name = $date_loop->format('dmY') == date('dmY') ? 'font-weight-bold bg-dark text-white rounded' : '';
        $class_name .= $date_loop->day == $date_current->day ? 'active bg-primary text-white rounded' : $class_name;
        $class_name .= $date_loop->day > date('j') ? 'text-muted rounded' : $class_name;
        $callback($class_name, $date_loop);
        $date_loop->addDay();
    }
}

function loopYears($date_current, $callback, $wide = 2)
{
    $year_current = $date_current->year;
    $year_now = intval(date('Y'));
    for ($i = $year_now + 1; $i >= $year_now - $wide; $i--) {
        $callback($i, $year_current, $date_current);
    }
}

function loopMonth($date_current, $callback)
{
    $date_start = new Carbon\Carbon("first day of {$date_current->format('Y')}");
    $date_start->startOfYear();
    for ($i = 1; $i <= 12; $i++) {
        $callback($date_start, $date_current);
        $date_start->addMonth();
    }
}
