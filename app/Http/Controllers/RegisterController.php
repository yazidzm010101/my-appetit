<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class RegisterController extends Controller
{
    /**
     * Login Page, if the user was authenticated then redirect to applications.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('auth.register');
    }


    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function registration(Request $request)
    {
        if($request->password == $request->repassword) {
            $post = [
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => Hash::make($request->password),
            ];
            if(User::create($post)){
                Auth::attempt(['email' => $request->email, 'password' => $request->password]);
            };
            return redirect('/app');
        }
        return back()->withErrors([
            'password' => 'Password tidak sama',
        ]);
    }
}
