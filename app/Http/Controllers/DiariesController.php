<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Diary;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use Exception;

class DiariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($date)
    {
        $user_id = Auth::id();
        $date_db = substr($date, 4, 4) . '-' . substr($date, 2, 2) . '-' . substr($date, 0, 2);
        $result = Diary::where('date', $date_db)
            ->where('user_id', $user_id)
            ->orderBy('date')
            ->orderBy('time')
            ->get()
            ->toArray();

        // $data['pagi'] = array_filter($result, function ($item) {
        //     $time = intval(str_replace(':', '', $item['time']));
        //     return $time >= 40000 && $time < 120000;
        // });

        // $data['siang'] = array_filter($result, function ($item) {
        //     $time = intval(str_replace(':', '', $item['time']));
        //     return $time >= 120000 && $time < 180000;
        // });

        // $data['malam'] = array_filter($result, function ($item) {
        //     $time = intval(str_replace(':', '', $item['time']));
        //     return $time >= 180000 || $time < 40000;
        // });


        return view('diaries.diaries-index', [
            'date_url' => $date,
            'date' => $date_db,
            // 'diaries' => $data,
            'diaries' => $result,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $date)
    {
        $request->validate([
            'food' => 'max:32|regex:/[A-Za-z0-9 ]+/',
            'amount' => 'regex:/[0-9]*[[:space:]]*/',
            'portion' => 'max:16|regex:/[A-Za-z]*[0-9]*[[:space:]]*/',
            'note' => 'max:255|regex:/[A-Za-z]*[0-9]*[[:space:]]*/',
        ]);

        $post = [
            'food'      => $request->food,
            'amount'    => $request->amount,
            'portion'   => $request->portion,
            'date'      => $request->date,
            'time'      => $request->time,
            'note'      => $request->note,
            'user_id'   => Auth::id(),
        ];

        if($filePhoto = $request->file('photo')) {
            $photo = $this->storePhoto($filePhoto, $post);
            $post['photo'] = $photo;
        }

        Diary::create($post);

        return redirect("app/diaries/$date");
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showPhoto(Request $request, $filename)
    {
        $storagePath = base_path("storage/app/private/photos/$filename");

        // echo($storagePath);
        return Image::make($storagePath)->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $date)
    {
        $post = [
            'id'        => $request->id,
            'food'      => $request->food,
            'amount'    => $request->amount,
            'portion'   => $request->portion,
            'date'      => $request->date,
            'time'      => $request->time,
            'note'      => $request->note,
            'user_id'   => Auth::id(),
        ];

        $diary = Diary::where('id', $post['id'])
            ->where('user_id', Auth::id())
            ->first();


        if($filePhoto = $request->file('photo')) {
            $photo = $this->storePhoto($filePhoto, $post);
            $post['photo'] = $photo;
        }

        unset($post['user_id']);

        if ($diary) {
            $diary->update($post);
        }

        return redirect("app/diaries/$date");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storePhoto($file, $diary)
    {
        try {
            // configuration
            $img_valid = ['jpg', 'jpeg', 'png'];
            $img_dir = base_path('storage/app/private/photos');
            $file_ext = $file->getClientOriginalExtension();
            $file_name = $diary['user_id'] . '_food_' . time();



            // validate if extension is valid
            if (!in_array(strtolower($file_ext), $img_valid)) {
                throw new Exception("File extension doesn't valid");
            }

            // create the image
            $img = Image::make($file->getRealPath());
            $img_thumb = Image::make($file->getRealPath());

            // crop image
            $img->fit(420, 240);
            $img->save("$img_dir/$file_name.$file_ext");

            // crop image
            $img_thumb->fit(64, 64);
            $img_thumb->save("$img_dir/{$file_name}_thumb.$file_ext");

            return $file_name . '.' . $file_ext;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
