<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DiariesController;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});


// here is the login
Route::prefix('login')->group(function() {
    Route::get('/', [LoginController::class, 'index'])->name('login');
    Route::post('/', [LoginController::class, 'authenticate']);
});

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

// here is register
Route::prefix('register')->group(function() {
    Route::get('/', [RegisterController::class, 'index'])->name('register');
    Route::post('/', [RegisterController::class, 'registration']);
});


// here is applications
Route::group([ 'middleware' => 'auth', 'prefix' => 'app'], function(){

    // if in index redirect to current date
    Route::get('/', function() {
        return redirect('app/diaries/' . date('dmY'));
    });

    Route::get('/photos/{filename}', [DiariesController::class, 'showPhoto']);

    // diaries
    Route::group(['prefix' => '/diaries'], function() {

        // if in index redirect to current date
        Route::get('/', function() {
            return redirect('app/diaries/' . date('dmY'));
        });

        // if has parameter
        Route::group(['prefix' => '/{date}', 'where' => ['date' => '[0-9]{8}']], function() {
            Route::get('/', [DiariesController::class, 'index']);
            Route::post('/add', [DiariesController::class, 'store']);
            Route::post('/update', [DiariesController::class, 'update']);
            Route::post('/delete', [DiariesController::class, 'destroy']);
        });
    });

    // recipes
});
